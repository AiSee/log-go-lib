package log

// todo: возможность блокировать повторные сообщения

import (
	"bytes"
	"fmt"
	stdLog "log"
	"log/syslog"
	"os"
	"regexp"
	"runtime"
	"sync"
	"time"
)

type message struct {
	text      string
	level     Level
	timestamp time.Time
}
type Level uint8
type logger struct{}

const (
	L_fatal Level = iota
	L_alert
	L_critical
	L_error
	L_warning
	L_notice
	L_info
	L_debug
)

var sys *syslog.Writer
var queue chan message
var maxConsoleLevel = L_debug
var maxSyslogLevel = L_debug
var maxFileLevel = L_debug
var maxMessagesPerSecond int
var thisSecond int64
var messagesThisSecond int
var logFile string
var loggingDisabled = false
var wg sync.WaitGroup
var myLogger *logger
var msgLevelCheck = regexp.MustCompile(`(?i)^([^\p{L}\p{M}\d]*` + // Любой регистр, любые не-буквы в начале
	`(fatal|alert|critical|crit|error|err|e|warning|warn|w|notice|info|debug)s?` + // Список уровней
	`[^\p{L}\p{M}\d_/-]+)`) // Хотя бы одна не-буква в конце

func (l Level) String() string {
	switch l {
	default:
		fallthrough
	case L_fatal:
		return "Fatal"
	case L_alert:
		return "Alert"
	case L_critical:
		return "Critical"
	case L_error:
		return "Error"
	case L_warning:
		return "Warning"
	case L_notice:
		return "Notice"
	case L_info:
		return "Info"
	case L_debug:
		return "Debug"
	}
}

func getText(args []interface{}, pattern string, level Level, file string, line int) string {
	var msg string
	if pattern == "" {
		msg = fmt.Sprint(args...)
	} else {
		msg = fmt.Sprintf(pattern, args...)
	}
	return fmt.Sprintf("[%s] %s:%d - %s\n", level, file, line, msg)
}

func logWorker() {
	for elem := range queue {
		if elem.level <= maxConsoleLevel {
			if elem.level <= L_warning {
				fmt.Fprint(os.Stderr, elem.timestamp.Format("2006-01-02 15:04:05 ")+elem.text)
			} else {
				fmt.Fprint(os.Stdout, elem.timestamp.Format("2006-01-02 15:04:05 ")+elem.text)
			}
		}
		if (sys != nil) && (elem.level <= maxSyslogLevel) {
			switch elem.level {
			default:
				fallthrough
			case L_fatal:
				sys.Emerg(elem.text)
			case L_alert:
				sys.Alert(elem.text)
			case L_critical:
				sys.Crit(elem.text)
			case L_error:
				sys.Err(elem.text)
			case L_warning:
				sys.Warning(elem.text)
			case L_notice:
				sys.Notice(elem.text)
			case L_info:
				sys.Info(elem.text)
			case L_debug:
				sys.Debug(elem.text)
			}
		}
		if (logFile != "") && (elem.level <= maxFileLevel) {
			f, err := os.OpenFile(logFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
			if err != nil {
				// println(err.Error())
				// Просто больше не будем пытаться туда писать
				logFile = ""
			} else {
				if _, err = f.WriteString(elem.timestamp.Format("2006-01-02 15:04:05 ") + elem.text); err != nil {
					// println(err.Error())
					logFile = ""
				}
				f.Close()
			}
		}
		wg.Done()
	}
}

// Возвращает true, если мы достигли лимита
func checkMessagesLimit() bool {
	if maxMessagesPerSecond < 1 {
		return false
	}
	if thisSecond != time.Now().Unix() {
		thisSecond = time.Now().Unix()
		messagesThisSecond = 1
		return false
	}
	if messagesThisSecond < maxMessagesPerSecond {
		messagesThisSecond++
		return false
	}
	return true
}

func addToQueue(pattern string, args []interface{}, level Level, depth int) {
	if (level > maxConsoleLevel && level > maxSyslogLevel && level > maxFileLevel) || loggingDisabled {
		return
	}
	if checkMessagesLimit() {
		return
	}
	_, file, line, _ := runtime.Caller(depth)
	short := file
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			short = file[i+1:]
			break
		}
	}
	text := getText(args, pattern, level, short, line)
	wg.Add(1)
	queue <- message{text, level, time.Now()}
}

func (t *logger) Write(buf []byte) (ln int, err error) {
	level := L_notice
	m := msgLevelCheck.FindSubmatch(buf)
	if len(m) == 3 {
		buf = bytes.Replace(buf, m[1], []byte(""), 1)
		letter := bytes.ToLower(m[2][0:1])
		switch letter[0] {
		case 'f':
			level = L_fatal
		case 'a':
			level = L_alert
		case 'c':
			level = L_critical
		case 'e':
			level = L_error
		case 'w':
			level = L_warning
		case 'n':
			level = L_notice
		case 'i':
			level = L_info
		case 'd':
			level = L_debug
		default:
		}
	}
	ln = len(buf)
	i := []interface{}{string(bytes.TrimSpace(buf))}
	addToQueue("", i, level, 4)
	if level == L_fatal {
		Die()
	}
	return
}

func CaptureStdLog() {
	stdLog.SetPrefix("")
	stdLog.SetFlags(0)
	stdLog.SetOutput(myLogger)
}

func RestoreStdLog() {
	stdLog.SetPrefix("")
	stdLog.SetFlags(stdLog.LstdFlags)
	stdLog.SetOutput(os.Stderr)
}

func InitSyslog(tag string, level Level) {
	maxSyslogLevel = level
	var err error
	sys, err = syslog.New(syslog.LOG_DEBUG|syslog.LOG_USER, tag)
	if err != nil {
		Error(err)
	}
}

func SetSyslogLevel(level Level) {
	maxConsoleLevel = level
}

func InitFile(file string, level Level) {
	logFile = file
	maxFileLevel = level
}

// maxLevel - Самый большой уровень логов, который будет записан
func SetConsoleLevel(level Level) {
	maxConsoleLevel = level
}

// Максимальное количество сообщений в секунду. Все сообщения выше лимита будут отброшены. 0 = нет лимита.
func SetMaxMessagesPerSecond(m int) {
	maxMessagesPerSecond = m
}

func Die() {
	loggingDisabled = true
	wg.Wait()
	os.Exit(-1)
}

func init() {
	queue = make(chan message, 100)
	CaptureStdLog()
	go logWorker()
}
