package log

func Fatal(args ...interface{}) {
	addToQueue("", args, L_fatal, 2)
	Die()
}

func Fatalf(pattern string, args ...interface{}) {
	addToQueue(pattern, args, L_fatal, 2)
	Die()
}

func Alert(args ...interface{}) {
	addToQueue("", args, L_alert, 2)
}

func Alertf(pattern string, args ...interface{}) {
	addToQueue(pattern, args, L_alert, 2)
}

func Critical(args ...interface{}) {
	addToQueue("", args, L_critical, 2)
}

func Criticalf(pattern string, args ...interface{}) {
	addToQueue(pattern, args, L_critical, 2)
}

func Error(args ...interface{}) {
	addToQueue("", args, L_error, 2)
}

func Errorf(pattern string, args ...interface{}) {
	addToQueue(pattern, args, L_error, 2)
}

func Warning(args ...interface{}) {
	addToQueue("", args, L_warning, 2)
}

func Warningf(pattern string, args ...interface{}) {
	addToQueue(pattern, args, L_warning, 2)
}

func Notice(args ...interface{}) {
	addToQueue("", args, L_notice, 2)
}

func Noticef(pattern string, args ...interface{}) {
	addToQueue(pattern, args, L_notice, 2)
}

func Info(args ...interface{}) {
	addToQueue("", args, L_info, 2)
}

func Infof(pattern string, args ...interface{}) {
	addToQueue(pattern, args, L_info, 2)
}

func Debug(args ...interface{}) {
	addToQueue("", args, L_debug, 2)
}

func Debugf(pattern string, args ...interface{}) {
	addToQueue(pattern, args, L_debug, 2)
}
